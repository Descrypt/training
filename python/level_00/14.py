'''
Expected output:
Yes
'''


x = 1
y = 2
z = 3

if x != 1:
    if y != 2:
        if z == 3:
            print('No')
        else:
            print('Yes')
    else:
        print('Nein')
else:
    print('Nee')
