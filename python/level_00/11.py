'''
expected output
1
3
5
7
'''


n = 0
while n != 7:
    n = n + 1
    if n % 2 != 0:
        n
