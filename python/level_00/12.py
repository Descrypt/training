'''
Expected output:
0
'''


operation = '-'

if operation == '+':
    print(3+3)
if operation == '-':
    print(3-3)
if operation == '*':
    print(3*3)
else:
    print(3/3)
