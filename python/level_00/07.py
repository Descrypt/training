'''
Expected output:
Bye world
'''


a = 10

if a == 10:
    print('Bye world')
else:
    print('Hello world')
