'''
Expected behaviour:
Make sure the "1" and "2" commands do as the program says.
'''


def list_friends(friends):
    for friend in friends:
        print(friend)


def add_friend(friends):
    new_friend = input('Enter new friends name: ')
    friends = friends + [new_friend]


friends = []
print('Press control-C to quit!')
while True:
    print('Enter 1 to list friends and 2 to add a new friend')
    command = input()
    if command == 1:
        list_friends(friends)
    elif command == 2:
        add_friend(friends)
    else:
        print('Unsupported command!')
