# Python training

The directories within the `python` directory all contain Python scripts. Every script contains one or more flaws. The goal is to get rid of said flaws. 

Be sure to make use of the provided code: if a script contains a variable `x`, you are expected to use this variable. 

> Feel free to make changes to existing code. Be sure to change and add, not remove.

These scripts have one of four problems:

1. The script generates an error message
2. The script does not produce the expected output (see below)
3. The script generates an error and does not produce the expected output (see below)
4. The script does not act as expected (see below)

If a script comes with expected output, this output is displayed in the top docstring of the document.

Example:

```
'''
Expected output:
100
'''


x = 100
```

If a script comes with expected behaviour, the expected behaviour is described in the top docstring of the document.

Example:

```
'''
Expected behaviour:
x is added to y.
'''


x - y
```

## Directory overview

### level 00
Topics:
- integers
- floating points
- strings
- booleans
- operators (`+ - / * ** // %`) 
- operands
- operator precedence
- variables
- type conversion (`str() int() float()`)
- comments (`#`)
- `if`
- `elif`
- `else`
- `while`
- `print()`
- `input()`
- `import`
- `from`

### Level 01
Topics:
- Everything listed at level 00
- lists
- `len()`
- `sorted()`
- iteration
- functions
- `def`
- `return`

### Level 02
Topics:
- Everything listed at level 01
- Dictionaries
- Using methods
- `.split()`

### Level 04
Topics:
- Everything listed at level 02
- `return` continued

### Level 05
Topics:
- Everythnig listed at level 03
- `class`
